# Skipping empty strings
(!$1) {next}

# Skipping IP addresses
(/([0-9]{1,3}\.){3}[0-9]{1,3}/) {next}

# Removing leading "www."
{gsub(/^www\./, "", $1)}

# Extracting base domain name
{
 if (/\.(ru|co|cu|com|info|net|org|gov|edu|int|mil|biz|pp|ne|msk|spb|nnov|od|in|ho|cc|dn|i|tut|v|dp|sl|ddns|duckdns|livejournal|herokuapp|azurewebsites|ucoz|3dn|nov)\.[^.]+$/)
  {$1 = gensub(/(.+)\.([^.]+\.[^.]+\.[^.]+$)/, "\\2", "")}
 else
  {$1 = gensub(/(.+)\.([^.]+\.[^.]+$)/, "\\2", "")}
}

d_all[$1]=$1

# Final function
END {
    asort(d_all)
    for (i in d_all) {
        print(d_all[i])
    }
}

